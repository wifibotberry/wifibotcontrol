#-------------------------------------------------
#
# Project created by QtCreator 2014-02-05T15:19:19
#
#-------------------------------------------------


#CONFIG BEGIN

#CONFIG +=VideoServerUseWia      #comment this line if wifibot running mjpg_streamer (linux), uncomment if using wia server (windows)
CONFIG +=XboxControllerSupport  #comment this line to disable xbox 360 controller support (avoir runtime errors when Xinput no installed)

DEFINES+="CAMERA_PORT=8080"
DEFINES+="BOT_PORT=15020"


#CONFIG END

QT       += core gui network widgets

TARGET = WifiBotControl
TEMPLATE = app


SOURCES +=  main.cpp\
            mainwindow.cpp \
            wifibot.cpp \
            MjpegClient.cpp \
    controlpad.cpp \
    videodisplay.cpp

HEADERS  += mainwindow.h \
            wifibot.h \
            MjpegClient.h \
    controlpad.h \
    videodisplay.h

FORMS    += mainwindow.ui

RESOURCES += screens.qrc \
            icons.qrc


win32:XboxControllerSupport {
    SOURCES +=  SimpleXbox360Controller/simplexbox360controller.cpp

    HEADERS  += SimpleXbox360Controller/XInput.h \
                SimpleXbox360Controller/simplexbox360controller.h

    LIBS += $${_PRO_FILE_PWD_}/SimpleXbox360Controller/XInput.lib

    DEFINES+= XBOX_CONTROLLER_SUPPORT
}

VideoServerUseWia:DEFINES+= CAMERA_USE_WIA

ANDROID_PACKAGE_SOURCE_DIR = $$PWD/android

OTHER_FILES += \
    android/AndroidManifest.xml
