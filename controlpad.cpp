#include "controlpad.h"
ControlPad::ControlPad( QWidget *parent) :
    QWidget(parent)
{


    gauche = new QPushButton(this);
    gauche->setText(QChar(0x2190));
    gauche->setSizePolicy(QSizePolicy::Minimum,QSizePolicy::Minimum);
    connect(gauche, SIGNAL(pressed()),this,SLOT(btnGauchePressed()));
    connect(gauche, SIGNAL(released()),this,SLOT(btnGaucheReleased()));

    droite = new QPushButton(this);
    droite->setText(QChar(0x2192));
    droite->setSizePolicy(QSizePolicy::Minimum,QSizePolicy::Minimum);
    connect(droite, SIGNAL(pressed()),this,SLOT(btnDroitPressed()));
    connect(droite, SIGNAL(released()),this,SLOT(btnDroitReleased()));

    haut = new QPushButton(this);
    haut->setText(QChar(0x2191));
    haut->setSizePolicy(QSizePolicy::Minimum,QSizePolicy::Minimum);
    connect(haut, SIGNAL(pressed()),this,SLOT(btnHautPressed()));
    connect(haut, SIGNAL(released()),this,SLOT(btnHautReleased()));
    bas = new QPushButton(this);
    bas->setText(QChar(0x2193));
    bas->setSizePolicy(QSizePolicy::Minimum,QSizePolicy::Minimum);
    connect(bas, SIGNAL(pressed()),this,SLOT(btnBasPressed()));
    connect(bas, SIGNAL(released()),this,SLOT(btnBasReleased()));

    central = new QPushButton(this);
    central->setSizePolicy(QSizePolicy::Minimum,QSizePolicy::Minimum);
    connect(central, SIGNAL(pressed()),this,SLOT(btnCentralPressed()));
    connect(central, SIGNAL(released()),this,SLOT(btnCentralReleased()));

    layout = new QGridLayout;
    layout->setSpacing(0);
    layout->setContentsMargins(0,0,0,0);

    layout->addWidget(gauche, 1, 0);
    layout->addWidget(droite, 1, 2);
    layout->addWidget(haut, 0, 1);
    layout->addWidget(central, 1, 1);
    layout->addWidget(bas, 2, 1);

    this->setLayout(layout);

}


ControlPad::ControlPad( QString textGauche, QString textDroite, QString textHaut, QString textBas, QString textCentral, QWidget *parent) :
    QWidget(parent)
{


    gauche = new QPushButton(this);
    gauche->setText(textGauche);
    gauche->setSizePolicy(QSizePolicy::Minimum,QSizePolicy::Minimum);
    connect(gauche, SIGNAL(pressed()),this,SLOT(btnGauchePressed()));
    connect(gauche, SIGNAL(released()),this,SLOT(btnGaucheReleased()));

    droite = new QPushButton(this);
    droite->setText(textDroite);
    droite->setSizePolicy(QSizePolicy::Minimum,QSizePolicy::Minimum);
    connect(droite, SIGNAL(pressed()),this,SLOT(btnDroitPressed()));
    connect(droite, SIGNAL(released()),this,SLOT(btnDroitReleased()));

    haut = new QPushButton(this);
    haut->setText(textHaut);
    haut->setSizePolicy(QSizePolicy::Minimum,QSizePolicy::Minimum);
    connect(haut, SIGNAL(pressed()),this,SLOT(btnHautPressed()));
    connect(haut, SIGNAL(released()),this,SLOT(btnHautReleased()));

    bas = new QPushButton(this);
    bas->setText(textBas);
    bas->setSizePolicy(QSizePolicy::Minimum,QSizePolicy::Minimum);
    connect(bas, SIGNAL(pressed()),this,SLOT(btnBasPressed()));
    connect(bas, SIGNAL(released()),this,SLOT(btnBasReleased()));

    central = new QPushButton(this);
    central->setText(textCentral);
    central->setSizePolicy(QSizePolicy::Minimum,QSizePolicy::Minimum);
    connect(central, SIGNAL(pressed()),this,SLOT(btnCentralPressed()));
    connect(central, SIGNAL(released()),this,SLOT(btnCentralReleased()));

    layout = new QGridLayout;

    layout->addWidget(gauche, 1, 0);
    layout->addWidget(droite, 1, 2);
    layout->addWidget(haut, 0, 1);
    layout->addWidget(central, 1, 1);
    layout->addWidget(bas, 2, 1);

    this->setLayout(layout);

}

void ControlPad::setCentralIcon(QIcon icon){
    central->setIcon(icon);
}

ControlPad::~ControlPad(){
    delete haut;
    delete gauche;
    delete bas;
    delete droite;
    if(central!=NULL)delete central;
}

