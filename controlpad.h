#ifndef CONTROLPAD_H
#define CONTROLPAD_H
#include <QPushButton>
#include <QGridLayout>
#include <QWidget>
#include <QIcon>

class ControlPad : public QWidget
{
    Q_OBJECT
public:
    enum enumActionPad { buttonGauchePressed, buttonDroitPressed, buttonHautPressed, buttonBasPressed, buttonCentralPressed, buttonGaucheReleased, buttonDroitReleased, buttonHautReleased, buttonBasReleased, buttonCentralReleased };

    explicit ControlPad(QWidget *parent);
    explicit ControlPad( QString textGauche, QString textDroite, QString textHaut, QString textBas, QString textCentral, QWidget *parent = 0);

    void setCentralIcon(QIcon icon);
    ~ControlPad();



signals:
    void pressed(ControlPad::enumActionPad action);

private slots:
    void btnGauchePressed(){emit pressed(buttonGauchePressed);}
    void btnDroitPressed(){emit pressed(buttonDroitPressed);}
    void btnHautPressed(){emit pressed(buttonHautPressed);}
    void btnBasPressed(){emit pressed(buttonBasPressed);}
    void btnCentralPressed(){emit pressed(buttonCentralPressed);}

    void btnGaucheReleased(){emit pressed(buttonGaucheReleased);}
    void btnDroitReleased(){emit pressed(buttonDroitReleased);}
    void btnHautReleased(){emit pressed(buttonHautReleased);}
    void btnBasReleased(){emit pressed(buttonBasReleased);}
    void btnCentralReleased(){emit pressed(buttonCentralReleased);}

private :
    QPushButton *gauche;
    QPushButton *droite;
    QPushButton *haut;
    QPushButton *bas;
    QPushButton *central;
    QGridLayout *layout;

};

#endif // CONTROLPAD_H
