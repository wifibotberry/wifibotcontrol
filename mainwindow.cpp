#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    myBot = new WifiBot();

    myMjpegClient = new MjpegClient();
    myMjpegClient->setAutoReconnect(false);

#if defined(XBOX_CONTROLLER_SUPPORT)
    x360Controller = new SimpleXbox360Controller();
    x360Controller->startAutoPolling(20);
#endif

#if !defined(CAMERA_USE_WIA)
    camControlSocket = new QTcpSocket(this);
#endif

    //init variables
    botUpPressed = false;
    botLeftPressed = false;
    botDownPressed = false;
    botRightPressed = false;

    //connect some stuffs
    connect(this->ui->pushButtonConnect,SIGNAL(clicked()),this,SLOT(botConnect()));
    connect(this->ui->pushButtonDisconnect,SIGNAL(clicked()),this,SLOT(botDisconnect()));
    connect(this->ui->lineEditHost,SIGNAL(returnPressed()),this,SLOT(botConnect()));
    connect(myBot,SIGNAL(connectionEstablished()),this,SLOT(botConnected()));
    connect(myBot,SIGNAL(connectionTerminated()),this,SLOT(botDisconnected()));
    connect(myBot,SIGNAL(connectionFailed()),this,SLOT(botConnectionFailed()));
    connect(myBot,SIGNAL(sensorsUpdate(WifiBot::Sensors)),this,SLOT(displaySensorsValues(WifiBot::Sensors)));
    connect(myMjpegClient,SIGNAL(socketError(QAbstractSocket::SocketError)),this,SLOT(cameraConnectionError(QAbstractSocket::SocketError)));
    connect(myMjpegClient,SIGNAL(newImage(QImage)),ui->cameraDisplayWidget,SLOT(displayImage(QImage)));
    connect(ui->padBot,SIGNAL(pressed(ControlPad::enumActionPad)),this,SLOT(padBotAction(ControlPad::enumActionPad)));
    connect(ui->padCamera,SIGNAL(pressed(ControlPad::enumActionPad)),this,SLOT(padCameraAction(ControlPad::enumActionPad)));

#if defined(XBOX_CONTROLLER_SUPPORT)
    connect(x360Controller,SIGNAL(controllerConnected(uint)),this,SLOT(gamepadConnected()));
    connect(x360Controller,SIGNAL(controllerDisconnected(uint)),this,SLOT(gamepadDisconnected()));
    connect(x360Controller,SIGNAL(controllerNewState(SimpleXbox360Controller::InputState)),this,SLOT(gamepadNewState(SimpleXbox360Controller::InputState)));
    connect(x360Controller,SIGNAL(controllerNewBatteryState(quint8,quint8)),this,SLOT(gamepadNewBatteryState(quint8,quint8)));
#endif
    connect(this,SIGNAL(connectionStateChanged(bool)),ui->checkBoxConnected,SLOT(setChecked(bool)));
    connect(qApp,SIGNAL(focusChanged(QWidget*,QWidget*)),this,SLOT(onApplicationFocusChanged()));

    //init UI
    ui->lineEditHost->setText("Bot Address");
    ui->pushButtonDisconnect->setEnabled(false);
    ui->comboBoxProtocol->addItem("Advanced Protocol",WifiBot::ADVANCED);
    ui->comboBoxProtocol->addItem("Simple Protocol",WifiBot::SIMPLE);
    ui->labelGamepad->setVisible(false);
    ui->labelGamepad->setPixmap(QPixmap(":/icons/xbox360controller"));
    ui->padBot->setCentralIcon(QIcon(":/icons/joystickIcon"));
    ui->padCamera->setCentralIcon(QIcon(":/icons/webcamIcon"));

    setWindowTitle("Wifibot Remote Control");
    setWindowIcon(QIcon(":/icons/icon"));
    setFocusPolicy(Qt::StrongFocus);

    cameraShowStandbyScreen();
}

void MainWindow::botConnect(){
    ui->cameraDisplayWidget->displayImage(QImage(":/screens/connecting"));
    WifiBot::ProtocolVersion protocol =(WifiBot::ProtocolVersion)ui->comboBoxProtocol->itemData(ui->comboBoxProtocol->currentIndex()).toInt();
    myBot->startConnection(ui->lineEditHost->text(),protocol);

}

void MainWindow::botDisconnect(){
    myBot->endConnection();
}

void MainWindow::botConnected(){

    ui->pushButtonConnect->setEnabled(false);
    ui->pushButtonDisconnect->setEnabled(true);
    ui->comboBoxProtocol->setEnabled(false);
    ui->lineEditHost->setEnabled(false);

#ifdef CAMERA_USE_WIA
    myMjpegClient->connectTo(myBot->getBotAddress(),CAMERA_PORT,"/cameras/1?q=40");
#else
    myMjpegClient->connectTo(myBot->getBotAddress(),CAMERA_PORT,"/?action=stream");
#endif
    emit connectionStateChanged(true);
}

void MainWindow::botDisconnected(){
    myMjpegClient->exit();
    cameraShowStandbyScreen();
    ui->pushButtonConnect->setEnabled(true);
    ui->pushButtonDisconnect->setEnabled(false);
    ui->comboBoxProtocol->setEnabled(true);
    ui->lineEditHost->setEnabled(true);

    emit connectionStateChanged(false);
}

void MainWindow::botConnectionFailed(){
    cameraShowStandbyScreen();
}

void MainWindow::cameraConnectionError(QAbstractSocket::SocketError error){
    if(error==QAbstractSocket::ConnectionRefusedError){
        ui->cameraDisplayWidget->displayImage(QImage(":/screens/novideo"));
    }
    else{
        ui->cameraDisplayWidget->displayImage(QImage(":/screens/videoerror"));
    }
}

void MainWindow::cameraShowStandbyScreen(){
    ui->cameraDisplayWidget->displayImage(QImage(":/screens/noconnection"));
}

void MainWindow::cameraCommands(QString expression){
#if defined(CAMERA_USE_WIA)
    Q_UNUSED(expression);
#else
    camControlSocket->connectToHost(QHostAddress(myBot->getBotAddress()),CAMERA_PORT);
    if(camControlSocket->waitForConnected(200)){
        char data[1024];

        sprintf(data, "GET %s HTTP/1.0\r\n",qPrintable(expression));
        camControlSocket->write((const char*)&data,strlen((const char*)data));

        sprintf(data, "Host: %s\r\n",qPrintable(myBot->getBotAddress()));
        camControlSocket->write((const char*)&data,strlen((const char*)data));

        sprintf(data, "\r\n");
        camControlSocket->write((const char*)&data,strlen((const char*)data));
        //QUrl camUrl("http://"+myBot->getBotAddress()+":"+QString::number(CAMERA_PORT)+expression);
        camControlSocket->disconnectFromHost();
    }
#endif
}

void MainWindow::displaySensorsValues(WifiBot::Sensors sensors){
    ui->lcdBattery->display(sensors.battery);
    ui->lcdCurrent->display(sensors.current);
    ui->lcdLSpeed->display(sensors.speedLeft);
    ui->lcdRSpeed->display(sensors.speedRight);
    ui->lcdLOdo->display(sensors.odoLeft);
    ui->lcdROdo->display(sensors.odoRight);
    ui->lcdADC0->display(sensors.adc0);
    ui->lcdADC1->display(sensors.adc1);
    ui->lcdADC3->display(sensors.adc3);
    ui->lcdADC4->display(sensors.adc4);
}

#if defined(XBOX_CONTROLLER_SUPPORT)
void MainWindow::gamepadNewState(SimpleXbox360Controller::InputState gamepadState){
    //bot commands
    sendPadCommandsToBot(gamepadState.leftThumbX,gamepadState.rightTrigger-gamepadState.leftTrigger);

    //camera commands
    if(gamepadState.isButtonPressed(XINPUT_GAMEPAD_A)){
        cameraCommands(CAMERA_RESET);
    }
    else if(gamepadState.isButtonPressed(XINPUT_GAMEPAD_DPAD_LEFT)){
        cameraCommands(CAMERA_LEFT);
    }
    else if(gamepadState.isButtonPressed(XINPUT_GAMEPAD_DPAD_RIGHT)){
        cameraCommands(CAMERA_RIGHT);
    }
    else if(gamepadState.isButtonPressed(XINPUT_GAMEPAD_DPAD_UP)){
        cameraCommands(CAMERA_UP);
    }
    else if(gamepadState.isButtonPressed(XINPUT_GAMEPAD_DPAD_DOWN)){
        cameraCommands(CAMERA_DOWN);
    }
}

void MainWindow::gamepadNewBatteryState(quint8 batteryType, quint8 batteryLevel){
    if(batteryType==BATTERY_TYPE_ALKALINE||batteryType==BATTERY_TYPE_NIMH){
        switch(batteryLevel){
        case BATTERY_LEVEL_FULL:
            ui->labelGamepad->setPixmap(QPixmap(":/icons/xbox360controllerBatteryFull"));
            break;
        case BATTERY_LEVEL_MEDIUM:
            ui->labelGamepad->setPixmap(QPixmap(":/icons/xbox360controllerBatteryMedium"));
            break;
        case BATTERY_LEVEL_LOW:
            ui->labelGamepad->setPixmap(QPixmap(":/icons/xbox360controllerBatteryLow"));
            break;
        case BATTERY_LEVEL_EMPTY:
            ui->labelGamepad->setPixmap(QPixmap(":/icons/xbox360controllerBatteryEmpty"));
            break;
        default:
            ui->labelGamepad->setPixmap(QPixmap(":/icons/xbox360controller"));
            break;
        }

    }
    else{
        ui->labelGamepad->setPixmap(QPixmap(":/icons/xbox360controller"));
    }
}

void MainWindow::gamepadConnected(){
    ui->labelGamepad->setVisible(true);
}

void MainWindow::gamepadDisconnected(){
    ui->labelGamepad->setVisible(false);
    myBot->setCommands(0,0);
}
#endif

void MainWindow::sendPadCommandsToBot(float x, float y){
    x=qBound(-1.0f,x,1.0f);
    y=qBound(-1.0f,y,1.0f);
    x*=qAbs(x)*BOTCOMMANDSMAX;
    y*=qAbs(y)*BOTCOMMANDSMAX;
    myBot->setCommands(y+x,y-x);
}

void MainWindow::sendKeyCommandsToBot(){
    if(botUpPressed&&!botDownPressed&&!botLeftPressed&&!botRightPressed){//avant
        myBot->setCommands(1000,1000);
    }
    else if(botUpPressed&&!botDownPressed&&botLeftPressed&&!botRightPressed){//avant gauche
        myBot->setCommands(500,1000);
    }
    else if(botUpPressed&&!botDownPressed&&!botLeftPressed&&botRightPressed){//avant droite
        myBot->setCommands(1000,500);
    }
    else if(!botUpPressed&&botDownPressed&&!botLeftPressed&&!botRightPressed){//arriere
        myBot->setCommands(-1000,-1000);
    }
    else if(!botUpPressed&&botDownPressed&&botLeftPressed&&!botRightPressed){//arriere gauche
        myBot->setCommands(-1000,-500);
    }
    else if(!botUpPressed&&botDownPressed&&!botLeftPressed&&botRightPressed){//arriere droite
        myBot->setCommands(-500,-1000);
    }
    else if(!botUpPressed&&!botDownPressed&&botLeftPressed&&!botRightPressed){//gauche
        myBot->setCommands(-1000,1000);
    }
    else if(!botUpPressed&&!botDownPressed&&!botLeftPressed&&botRightPressed){//droite
        myBot->setCommands(1000,-1000);
    }
    else{//arret
        myBot->setCommands(0,0);
    }
}

void MainWindow::keyPressEvent(QKeyEvent *event){
    switch(event->key()){
    //bot commands
    case Qt::Key_Z:
        botUpPressed=true;
        sendKeyCommandsToBot();
        break;
    case Qt::Key_S:
        botDownPressed=true;
        sendKeyCommandsToBot();
        break;
    case Qt::Key_D:
        botRightPressed=true;
        sendKeyCommandsToBot();
        break;
    case Qt::Key_Q:
        botLeftPressed=true;
        sendKeyCommandsToBot();
        break;
    case Qt::Key_Control:
        cameraCommands(CAMERA_RESET);
        break;
    case Qt::Key_Up:
        cameraCommands(CAMERA_UP);
        break;
    case Qt::Key_Left:
        cameraCommands(CAMERA_LEFT);
        break;
    case Qt::Key_Down:
        cameraCommands(CAMERA_DOWN);
        break;
    case Qt::Key_Right:
        cameraCommands(CAMERA_RIGHT);
        break;
    }
}

void MainWindow::keyReleaseEvent(QKeyEvent *event){
    switch(event->key()){
    case Qt::Key_Z:
        botUpPressed=false;
        break;
    case Qt::Key_S:
        botDownPressed=false;
        break;
    case Qt::Key_D:
        botRightPressed=false;
        break;
    case Qt::Key_Q:
        botLeftPressed=false;
        break;
    case Qt::Key_Back:
        QCoreApplication::quit();
        break;
    }
    sendKeyCommandsToBot();
}

void MainWindow::padBotAction(ControlPad::enumActionPad action){
    switch(action){
    case ControlPad::buttonHautPressed:
        botUpPressed=true;
        break;
    case ControlPad::buttonHautReleased:
        botUpPressed=false;
        break;
    case ControlPad::buttonGauchePressed:
        botLeftPressed=true;
        break;
    case ControlPad::buttonGaucheReleased:
        botLeftPressed=false;
        break;
    case ControlPad::buttonBasPressed:
        botDownPressed=true;
        break;
    case ControlPad::buttonBasReleased:
        botDownPressed=false;
        break;
    case ControlPad::buttonDroitPressed:
        botRightPressed=true;
        break;
    case ControlPad::buttonDroitReleased:
        botRightPressed=false;
        break;
    case ControlPad::buttonCentralPressed:
        botUpPressed=false;
        botLeftPressed=false;
        botDownPressed=false;
        botRightPressed=false;
        break;
    default :
        break;
    }
    sendKeyCommandsToBot();
}

void MainWindow::padCameraAction(ControlPad::enumActionPad action){
    switch(action){
    case ControlPad::buttonCentralPressed:
        cameraCommands(CAMERA_RESET);
        break;
    case ControlPad::buttonHautPressed:
        cameraCommands(CAMERA_UP);
        break;
    case ControlPad::buttonGauchePressed:
        cameraCommands(CAMERA_LEFT);
        break;
    case ControlPad::buttonBasPressed:
        cameraCommands(CAMERA_DOWN);
        break;
    case ControlPad::buttonDroitPressed:
        cameraCommands(CAMERA_RIGHT);
        break;
    default:
        break;
    }
}

void MainWindow::onApplicationFocusChanged(){
    if(!isActiveWindow()&&(botUpPressed||botLeftPressed||botDownPressed||botRightPressed)){
        botUpPressed=false;
        botLeftPressed=false;
        botDownPressed=false;
        botRightPressed=false;
        sendKeyCommandsToBot();
    }
}

MainWindow::~MainWindow()
{
    delete ui;
    delete myBot;
    delete myMjpegClient;
#if defined(XBOX_CONTROLLER_SUPPORT)
    delete x360Controller;
#endif

#if !defined(CAMERA_USE_WIA)
    delete camControlSocket;
#endif
}
