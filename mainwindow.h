#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTcpSocket>
#include <QByteArray>
#include <QTimer>
#include <QKeyEvent>
#include <QPixmap>
#include <QImage>
#include <QResizeEvent>
#include <QAbstractSocket>
#include <QIcon>
#include <QNetworkAccessManager>
#include <QNetworkRequest>

#include "wifibot.h"
#include "MjpegClient.h"
#include "controlpad.h"

#ifdef XBOX_CONTROLLER_SUPPORT
    #include "SimpleXbox360Controller/simplexbox360controller.h"
#endif

#define CAMERA_UP "/?action=command&dest=0&plugin=0&id=10094853&group=1&value=-200"
#define CAMERA_DOWN "/?action=command&dest=0&plugin=0&id=10094853&group=1&value=200"
#define CAMERA_LEFT "/?action=command&dest=0&plugin=0&id=10094852&group=1&value=200"
#define CAMERA_RIGHT "/?action=command&dest=0&plugin=0&id=10094852&group=1&value=-200"
#define CAMERA_RESET "/?action=command&dest=0&plugin=0&id=168062211&group=1&value=3"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
signals:
    void connectionStateChanged(bool);
public slots:
    void botConnect();
    void botDisconnect();
private slots:
    //wifibot
    void botConnected();
    void botDisconnected();
    void botConnectionFailed();
    void displaySensorsValues(WifiBot::Sensors sensors);

    //camera
    void cameraConnectionError(QAbstractSocket::SocketError error);

    //Camera control
    void cameraCommands(QString expression);

#if defined(XBOX_CONTROLLER_SUPPORT) //gamepad
    void gamepadConnected();
    void gamepadDisconnected();
    void gamepadNewState(SimpleXbox360Controller::InputState gamepadState);
    void gamepadNewBatteryState(quint8 batteryType, quint8 batteryLevel);
#endif

    //application
    void onApplicationFocusChanged();

    //UI controlPads
    void padBotAction(ControlPad::enumActionPad action);
    void padCameraAction(ControlPad::enumActionPad action);

private:
    Ui::MainWindow *ui;
    WifiBot *myBot;
    MjpegClient *myMjpegClient;

#if !defined(CAMERA_USE_WIA)
    QTcpSocket *camControlSocket;
#endif

#if defined(XBOX_CONTROLLER_SUPPORT)
    SimpleXbox360Controller *x360Controller;
#endif
    bool botUpPressed;
    bool botDownPressed;
    bool botLeftPressed;
    bool botRightPressed;

    void sendKeyCommandsToBot(void);
    void sendPadCommandsToBot(float x, float y);
    void cameraShowStandbyScreen(void);

protected:
    void keyReleaseEvent(QKeyEvent *event);
    void keyPressEvent(QKeyEvent *event);
};

#endif // MAINWINDOW_H
