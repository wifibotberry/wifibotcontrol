 Mjpeg client from https://code.google.com/p/securitycamviewer/ under GNU GPL v2 License
 Xcontroller original project from https://code.google.com/p/xcontroller/
 windows icon from http://soundforge.deviantart.com/
 xbox 360 controller icon from SvenGraph http://www.svengraph.net
 battery icons from Oxygen Team http://www.oxygen-icons.org/
 joystick / camera icons from Farm-fresh set, by FatCow Web Hosting http://www.fatcow.com/
 
 
 ==Xbox360 controller key bindings==
 triggers = forward / backward
 left stick : direction
 D-PAD : camera movement
 A : reset camera