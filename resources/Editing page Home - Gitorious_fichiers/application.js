this.gts.datePicker = (function() {

  return function(input) {
    $(input).datepicker({ dateFormat: "yy-mm-dd" });
  };

}());
this.gts.deleteLinks = (function() {

  return function(table) {
    var $table = $(table);

    $table.find('a.btn-danger').on('ajax:success', function(e) {
      e.preventDefault();
      var $el = $(this);
      $tr = $el.parents('tr');
      $tr.fadeOut('fast', function() { $tr.remove(); });
    });

    $table.find('a.btn-danger').on('click', function(e) {
      e.preventDefault();

      if (confirm('Are you sure?')) {
        var $el = $(this);

        $.ajax({
          url: this.href,
          method: 'delete',
          success: function(e) { $el.trigger('ajax:success'); }
        });
      }
    });
  };

}());
this.gts.expandableTextarea = (function() {

  return function(textarea) {
    $el = $(textarea);

    $el.on('focus', function(e) {
      $(this).removeClass('gts-textarea-expandable');
    });

    $el.on('blur', function(e) {
      if (this.value) return;
      $(this).addClass('gts-textarea-expandable');
    });
  };

}());
this.gts.issueAssigneeAutocomplete = (function() {

  return function(input, assignees_json) {
    var assignees = $.parseJSON(assignees_json);

    var $input = $(input);
    var $list  = $input.parents('.issue-assignee-widget').find('.issue-assignees');

    function createButton(item) {
      var value, label;

      value = item.id;
      label = item.label;

      var $li = $('<li/>');

      $li.append(
        $('<button class="btn"></button>')
          .val(value).html('<i class="icon-remove"></i>'+label).addClass('btn')
      );

      $li.append(
        $('<input type="hidden" name="issue[assignee_ids][]"/>').val(value)
      );

      return $li;
    }

    function appendAssignee(item) {
      $list.append(createButton(item));
    }

    function findUser() {
      var login = $input.val();
      return cull.first(function(el) { return el.label == login }, assignees);
    }

    function onConfirm() {
      var item = $input.data('item');
      if (!item) item = findUser();
      if (item) {
        appendAssignee(item);
        $input.val('');
      }
    }

    $input.next('a[data-behavior="assign-user"]').click(function(e) {
      e.preventDefault();
      onConfirm();
    });

    $input.keydown(function(e) {
      if (e.which == 13) {
        e.preventDefault();
        onConfirm();
      }
    });

    $list.on('click', 'button', function(e) {
      e.preventDefault();
      $(this).parent().remove();
    });

    $input.autocomplete({
      source: assignees,
      appendTo: $input.parents('.autocomplete-widget'),
      select: function(e, ui) {
        var item = ui.item;
        $input.val(item.label);
        $input.data('item', item);
        onConfirm();
        e.preventDefault();
      }
    });
  };

}());
this.gts.issueFilters = (function() {

  return function(form) {
    var $form = $(form);

    $form.on('change', '.btn-label input', function(e) {
      var $chk = $(this);
      var $btn = $chk.parent();
      $btn.toggleClass('btn-inactive', !$chk.prop('checked'));
    });

    $form.on('click', '[data-behavior=submit-buttons] a', function(e) {
      e.preventDefault();

      var $btn = $(this);
      var url  = $btn.attr('href');

      if ($btn.data('behavior') == 'filter') {
        url = [url, '?', $form.serialize()].join('');
      }

      $.pjax({ url: url, container: '#gts-pjax-container' });
    });
  };

}());
this.gts.issueLabelAutocomplete = (function() {

  return function(input, labels_json) {
    var labels = $.parseJSON(labels_json);

    var $input = $(input);
    var $list  = $input.parents('.issue-label-widget').find('.issue-labels');

    function createButton(item) {
      var value, label, color, $li;

      value = item.id;
      label = item.label;
      color = item.color;

      $li = $('<li/>');

      $li.append(
        $('<button class="btn btn-mini btn-label"></button>')
          .css({ backgroundColor: color })
          .html('<i class="icon-remove icon-white"></i>'+label)
          .attr('id', 'label-'+value)
      );

      $li.append(
        $('<input type="hidden" name="issue[label_ids][]"/>').val(value)
      );

      return $li;
    }

    function appendLabel(item) {
      $list.append(createButton(item));
    }

    function findLabel() {
      var name = $input.val();
      return cull.first(function(el) { return el.label == name }, labels);
    }

    function onConfirm() {
      var item = $input.data('item');
      if (!item) item = findLabel();
      if (item) {
        if ($list.find('#label-'+item.id).length > 0) return;
        appendLabel(item);
        $input.val('');
      }
    }

    $input.next('a[data-behavior="add-label"]').click(function(e) {
      e.preventDefault();
      onConfirm();
    });

    $input.keydown(function(e) {
      if (e.which == 13) {
        e.preventDefault();
        onConfirm();
        $input.autocomplete('close');
      }
    });

    $input.autocomplete({
      source: labels,
      appendTo: $input.parents('.autocomplete-widget'),
      select: function(e, ui) {
        var item = ui.item;
        $input.val(item.label);
        $input.data('item', item);
        onConfirm();
        e.preventDefault();
      }
    });

    $list.on('click', 'button', function(e) {
      e.preventDefault();
      $(this).parent().remove();
    });
  };

}());

//

this.gts = this.gts || {};

gts.app.feature('expandable-textarea', gts.expandableTextarea, {
  elements: ['gts-textarea-expandable']
});

gts.app.feature('delete-links', gts.deleteLinks, {
  elements: ['gts-delete-links']
});

gts.app.feature('issue-assignee-autocomplete', gts.issueAssigneeAutocomplete, {
  elements: ['gts-issue-assignee-candidate'],
  depends: ['issue-assignee-candidates']
});

gts.app.feature('issue-label-autocomplete', gts.issueLabelAutocomplete, {
  elements: ['gts-issue-label'],
  depends: ['project-labels']
});

gts.app.feature('issue-filters', gts.issueFilters, {
  elements: ['gts-issue-filters']
});

gts.app.feature('date-picker', gts.datePicker, {
  elements: ['gts-date-picker']
});

$(function() {
  $('body').on('click', 'a[data-gts-toggle]', function(e) {
    e.preventDefault();

    var $this = $(this);
    var $el = $($this.data('gts-toggle'));

    $el.toggle();

    if ($this.data('gts-toggle-type') == 'pullbox') {
      $el.parents('.pull-box-container').toggleClass('closed');
    }
  });
});
