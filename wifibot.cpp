#include "wifibot.h"

WifiBot::WifiBot(QObject *parent) :
    QObject(parent)
{
    protocol = SIMPLE;
    pidMode = PID_on_50ms;

    tcpSocket = new QTcpSocket(this);

    sendingTimer = new QTimer(this);
    sendingTimer->setInterval(RETRANSMITINTERVAL);

    sendingByteArray = new QByteArray();

    connect(sendingTimer, SIGNAL(timeout()),this,SLOT(communicateWithBot()));
    connect(tcpSocket,SIGNAL(connected()),this,SLOT(socketConnected()));
    connect(tcpSocket,SIGNAL(disconnected()),this,SLOT(socketDisconnected()));
}

void WifiBot::startConnection(QString botHost, ProtocolVersion communicationProtocol){
    protocol= communicationProtocol;

    //establishing connection
    tcpSocket->connectToHost(botHost,BOT_PORT);
    if(tcpSocket->waitForConnected(CONNECTIONWAITDELAY)){
        setCommands(0,0);
        sendingTimer->start();
    }
    else{
        emit connectionFailed();
    }
}

void WifiBot::endConnection(){
    tcpSocket->disconnectFromHost();
}

void WifiBot::setPIDMode(PIDSettings setting){
    pidMode = setting;
}

void WifiBot::setCommands(int Lspeed, int Rspeed){
    //bounding parameters
    quint32 Lvalue=qMin(qAbs(Lspeed),BOTCOMMANDSMAX);
    quint32 Rvalue=qMin(qAbs(Rspeed),BOTCOMMANDSMAX);

    sendingByteArray->clear();

    switch(protocol){
    case SIMPLE:
        sendingByteArray->append((Lspeed<0?0b10000000:0b11000000)|(0b00111111&(char)(Lvalue*63/BOTCOMMANDSMAX)));
        sendingByteArray->append((Rspeed<0?0b10000000:0b11000000)|(0b00111111&(char)(Rvalue*63/BOTCOMMANDSMAX)));
        break;

    case ADVANCED:
        sendingByteArray->append((char)0xff);
        sendingByteArray->append((char)0x07);
        sendingByteArray->append((char)(Lvalue*240/BOTCOMMANDSMAX));
        sendingByteArray->append((char)0x00);
        sendingByteArray->append((char)(Rvalue*240/BOTCOMMANDSMAX));
        sendingByteArray->append((char)0x00);
        sendingByteArray->append(((char)pidMode&0b10101000)|(Lspeed<0?0b00000000:0b01000000)|(Rspeed<0?0b00000000:0b00010000));

        quint16 crcValue = crc16(sendingByteArray,1);

        sendingByteArray->append((char)(crcValue));
        sendingByteArray->append((char)(crcValue>>8));
        break;
    }
}

void WifiBot::communicateWithBot(){
    tcpSocket->write(*sendingByteArray);
    //debugTransmission();
    readSensorsData();
}

void WifiBot::readSensorsData(){
    switch(protocol){
    case SIMPLE:
        while(tcpSocket->bytesAvailable()>=7){
            QDataStream sensorsStream(tcpSocket);
            quint8 battery,speedFrontL, speedRearL,speedFrontR,speedRearR,IrLeft, IrRight;
            sensorsStream>>battery>>speedFrontL>>speedRearL>>speedFrontR>>speedRearR>>IrLeft>>IrRight;

            WifiBot::Sensors sensors;

            sensors.battery=battery;
            sensors.speedLeft=speedFrontL/2 + speedRearL/2;
            sensors.speedRight=speedFrontR/2 + speedRearR/2;
            sensors.adc4=IrLeft;
            sensors.adc0=IrRight;
            //sensors.printToDebug();

            emit sensorsUpdate(sensors);
        }
        break;
    case ADVANCED:
        while(tcpSocket->bytesAvailable()>=21){
            QByteArray sensorsArray = tcpSocket->read(21);
            QDataStream sensorsStream(sensorsArray);

            sensorsStream.setByteOrder(QDataStream::LittleEndian);
            quint32 odoL,odoR;
            qint16 speedL,speedR;
            quint16 crc;
            quint8 battery,adc0,adc1,adc3,adc4,current,dummy;
            sensorsStream>>speedL>>battery>>adc4>>adc3>>odoL>>speedR>>adc0>>adc1>>odoR>>current>>dummy>>crc;

            WifiBot::Sensors sensors;

            sensors.battery=(float)battery*BATTERYCONVERSIONVALUE;

            if(pidMode==PID_on_10ms||pidMode==PID_off_10ms){    //If speed is tick per 10ms
                sensors.speedLeft=((float)speedL)*100.0f/TICKPERMETER;
                sensors.speedRight=((float)speedR)*100.0f/TICKPERMETER;
            }
            else{                                               //else speed is tick per 50ms
                sensors.speedLeft=((float)speedL)*20.0f/TICKPERMETER;
                sensors.speedRight=((float)speedR)*20.0f/TICKPERMETER;
            }

            sensors.odoLeft=((float)odoL)/TICKPERMETER;
            sensors.odoRight=((float)odoR)/TICKPERMETER;
            sensors.current=((float)current)*CURRENTCONVERSIONVALUE;
            sensors.adc4=adc4*ADCCONVERSIONVALUE;
            sensors.adc3=adc3*ADCCONVERSIONVALUE;
            sensors.adc0=adc0*ADCCONVERSIONVALUE;
            sensors.adc1=adc1*ADCCONVERSIONVALUE;
            //sensors.printToDebug();

            emit sensorsUpdate(sensors);
        }
        break;
    }
}

quint16 WifiBot::crc16(QByteArray* byteArray, int pos){
    quint16 crc = 0xFFFF;
    quint16 Polynome = 0xA001;
    quint16 Parity = 0;
    for(; pos < byteArray->length(); pos++){
        crc ^= (unsigned char)byteArray->at(pos);
        for (unsigned int CptBit = 0; CptBit <= 7 ; CptBit++){
            Parity= crc;
            crc >>= 1;
            if (Parity%2 == true) crc ^= Polynome;
        }
    }
    return crc;
}

void WifiBot::socketConnected(){
    emit connectionEstablished();
}

void WifiBot::socketDisconnected(){
    sendingTimer->stop();
    sendingByteArray->clear();
    emit connectionTerminated();
    Sensors zeroedSensors;
    emit sensorsUpdate(zeroedSensors);
}

void WifiBot::debugTransmission(){
    for(int i = 0; i<sendingByteArray->size() ;i++){
        qDebug()<<QString::number((unsigned char)sendingByteArray->at(i),16);
    }
    qDebug()<<"==========================================";
}

QString WifiBot::getBotAddress(void){
    return tcpSocket->peerAddress().toString();
}

WifiBot::ProtocolVersion WifiBot::getProtocolVersion(void){
    return protocol;
}

WifiBot::~WifiBot(){
    delete tcpSocket;
    delete sendingTimer;
    delete sendingByteArray;
}
