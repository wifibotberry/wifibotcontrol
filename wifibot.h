#ifndef WIFIBOT_H
#define WIFIBOT_H

#include <QObject>
#include <QTcpSocket>
#include <QString>
#include <QTimer>
#include <QByteArray>
#include <QDataStream>
#include <QHostAddress>

#define BOTCOMMANDSMAX          1000
#define RETRANSMITINTERVAL      20
#define CONNECTIONWAITDELAY     1000

#define TICKPERMETER            5564.0f
#define ADCCONVERSIONVALUE      3.3/255
#define BATTERYCONVERSIONVALUE  0.1f
#define CURRENTCONVERSIONVALUE  0.1f

class WifiBot : public QObject
{
    Q_OBJECT
public:

    class Sensors {
    public:
        Sensors():speedLeft(0),speedRight(0),odoLeft(0),odoRight(0),battery(0),current(0),adc0(0),adc1(0),adc3(0),adc4(0){}
        void printToDebug(void){qDebug()<<"Bat="<<battery<<"LSpeed="<<speedLeft<<"RSpeed="<<speedRight<<"Lodo="<<odoLeft<<"Rodo="<<odoRight<<"Current="<<current<<"ADCs="<<adc0<<adc1<<adc3<<adc4;}
        float speedLeft;
        float speedRight;
        float odoLeft;
        float odoRight;
        float battery;
        float current;
        float adc0;
        float adc1;
        float adc3;
        float adc4;
    };

    enum ProtocolVersion{SIMPLE, ADVANCED};
    enum PIDSettings{PID_off_50ms=0b00000000,PID_off_10ms=0b00001000,PID_on_50ms=0b10100000,PID_on_10ms=0b10101000};

    explicit WifiBot(QObject *parent = 0);
    ~WifiBot();
    
    QString getBotAddress(void);
    ProtocolVersion getProtocolVersion(void);
signals:
    void connectionEstablished(void);
    void connectionTerminated(void);
    void connectionFailed(void);
    void sensorsUpdate(WifiBot::Sensors sensorsValue);

public slots:
    void startConnection(QString botHost,ProtocolVersion communicationProtocol = SIMPLE);
    void endConnection();
    void setCommands(int Lspeed,int Rspeed);
    void setPIDMode(PIDSettings setting);

private slots:
    void communicateWithBot();
    void socketConnected(void);
    void socketDisconnected(void);

private:
    QTcpSocket* tcpSocket;
    QByteArray* sendingByteArray;
    QTimer* sendingTimer;
    ProtocolVersion protocol;
    PIDSettings pidMode;

    static quint16 crc16(QByteArray *byteArray, int pos);
    void readSensorsData(void);

    void debugTransmission(void);
};

#endif // WIFIBOT_H
